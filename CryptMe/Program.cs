﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;

namespace CryptMe
{
    class Program
    {
        public const string SUFFIXE = ".epsilon0";
        public const string SUFFIXE_FILE = SUFFIXE + ".encrypted";
        public const string SUFFIXE_KEY = SUFFIXE + ".key";
        public const string SUFFIXE_IV = SUFFIXE + ".iv";

        static void Main(string[] args)
        {
            Console.OutputEncoding = System.Text.Encoding.UTF8;
            while (true)
            {
                Console.WriteLine("Que voulez vous faire ?");
                Console.WriteLine("1) Crypter");
                Console.WriteLine("2) Décrypter");
                Console.WriteLine("3) Quitter");
                string choix = Console.ReadLine();

                Console.WriteLine("\n\n");

                switch (choix)
                {
                    case "1":
                        string choix_methode = ChoixDonnee();
                        switch(choix_methode)
                        {
                            case "1":
                                InitEncryptionFile();
                                break;
                            case "2":
                                InitEncryptionFolder();
                                break;
                        }
                        break;
                    case "2":
                        string choix_methode_dec = ChoixDonnee();
                        switch (choix_methode_dec)
                        {
                            case "1":
                                InitDecryptionFile();
                                break;
                            case "2":
                                InitDecryptionFolder();
                                break;
                        }
                        break;
                    case "3":
                        return;
                    default:
                        Console.WriteLine("Choix non valide\n\n");
                        break;
                }
            }

        }

        static string ChoixDonnee()
        {
            Console.WriteLine("Quelle type de données ?");
            Console.WriteLine("1) Fichier");
            Console.WriteLine("2) Répertoire");
            Console.WriteLine("3) Retour");
            return Console.ReadLine();
        }

        static void InitEncryptionFile()
        {
            Console.Write("Path du fichier à cryper : ");
            string filename = Console.ReadLine();

            if (File.Exists(filename))
            {
                if (!File.Exists(filename + SUFFIXE_FILE))
                {
                    bool response = EncryptFile(filename);
                    Console.WriteLine(response);
                    if (response)
                    {
                        File.Delete(filename);
                    }
                }
                else
                {
                    Console.WriteLine("Le fichier est déjà crypté");
                }
            }
            else
            {
                Console.WriteLine("Le fichier n'existe pas");
            }

        }

        static void InitEncryptionFolder()
        {
            Console.Write("Path dossier à cryper : ");
            string filename = Console.ReadLine();

            if (Directory.Exists(filename))
            {
                if (!Directory.Exists(filename+SUFFIXE_FILE))
                {
                    using (Aes aesAlg = Aes.Create())
                    {
                        Directory.CreateDirectory(filename + SUFFIXE_FILE);
                        bool response = EncryptFolder(filename, filename + SUFFIXE_FILE, aesAlg);
                        Console.WriteLine(response);
                        byte[] key = aesAlg.Key;
                        byte[] IV = aesAlg.IV;

                        try
                        {
                            using (var fs = new FileStream(filename + SUFFIXE_FILE + "/" + SUFFIXE_KEY, FileMode.Create, FileAccess.Write))
                            {
                                fs.Write(key, 0, key.Length);
                            }
                            using (var fs = new FileStream(filename + SUFFIXE_FILE + "/" + SUFFIXE_IV, FileMode.Create, FileAccess.Write))
                            {
                                fs.Write(IV, 0, IV.Length);
                            }
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("Exception caught in process: {0}", ex);
                        }

                        if (response)
                        {
                            Directory.Delete(filename, true);
                        }
                    }
                    
                }
                else
                {
                    Console.WriteLine("Le dossier est déjà chiffré.");
                }
            }
            else
            {
                Console.WriteLine("Le dossier n'existe pas.");
            }

        }

        static void InitDecryptionFile()
        {
            Console.Write("Nom du fichier à décrypter : ");
            string filename = Console.ReadLine();

            if (filename.EndsWith(SUFFIXE_FILE))
            {
                filename = filename.Split(new[] { SUFFIXE_FILE }, StringSplitOptions.None)[0];
            }

            if (File.Exists(filename + SUFFIXE_FILE) && !File.Exists(filename))
            {
                if (File.Exists(filename + SUFFIXE_KEY) && File.Exists(filename + SUFFIXE_IV))
                {
                    bool response = DecryptFile(filename);
                    Console.WriteLine(response);
                    if (response)
                    {
                        File.Delete(filename + SUFFIXE_KEY);
                        File.Delete(filename + SUFFIXE_FILE);
                        File.Delete(filename + SUFFIXE_IV);
                    }
                }
                else
                {
                    Console.WriteLine("Il manque la clé ou le vecteur d'initialisation");
                }
            }
            else
            {
                Console.WriteLine("Le fichier n'existe pas ou est déjà décrypté");
            }

        }

        static void InitDecryptionFolder()
        {
            Console.Write("Path dossier à décrypter : ");
            string filename = Console.ReadLine();

            if (filename.EndsWith("/") || filename.EndsWith("\\"))
            {
                filename = filename.Remove(filename.Length - 1);
            }

            if (filename.EndsWith(SUFFIXE_FILE))
            {
                filename = filename.Split(new[] { SUFFIXE_FILE }, StringSplitOptions.None)[0];
            }

            if (Directory.Exists(filename + SUFFIXE_FILE) && !Directory.Exists(filename))
            {
                if (File.Exists(filename + SUFFIXE_FILE + "/" + SUFFIXE_KEY) && File.Exists(filename + SUFFIXE_FILE + "/" + SUFFIXE_IV))
                {
                    using (Aes aesAlg = Aes.Create())
                    {
                        Directory.CreateDirectory(filename);
                        byte[] key = File.ReadAllBytes(filename + SUFFIXE_FILE + "/" + SUFFIXE_KEY);
                        byte[] IV = File.ReadAllBytes(filename + SUFFIXE_FILE + "/" + SUFFIXE_IV);

                        try
                        {
                            aesAlg.Key = key;
                            aesAlg.IV = IV;
                            bool response = DecryptFolder(filename, filename + SUFFIXE_FILE, aesAlg);
                            Console.WriteLine(response);
                            if (response)
                            {
                                Directory.Delete(filename + SUFFIXE_FILE, true);
                            }
                        } catch (Exception ex)
                        {
                            Console.WriteLine(filename + SUFFIXE_FILE + "/" + SUFFIXE_KEY);
                            Console.WriteLine("La clé ou le vecteur n'est pas au bon format.");
                            Console.WriteLine("Exception caught in process: {0}", ex);
                        }
                        

                    }

                }
                else
                {
                    Console.WriteLine("Il manque la clé ou le vecteur d'initialisation");
                }
            }
            else
            {
                Console.WriteLine("Le dossier n'existe pas ou est déjà décrypté");
            }

        }

        static Boolean EncryptFile(string filename)
        {
            using (Aes aesAlg = Aes.Create())
            {
                byte[] encrypted = Encrypt(aesAlg, filename);
                byte[] key = aesAlg.Key;
                byte[] IV = aesAlg.IV;
                try
                {
                    using (var fs = new FileStream(filename + SUFFIXE_FILE, FileMode.Create, FileAccess.Write))
                    {
                        fs.Write(encrypted, 0, encrypted.Length);
                    }
                    using (var fs = new FileStream(filename + SUFFIXE_KEY, FileMode.Create, FileAccess.Write))
                    {
                        fs.Write(key, 0, key.Length);
                    }
                    using (var fs = new FileStream(filename + SUFFIXE_IV, FileMode.Create, FileAccess.Write))
                    {
                        fs.Write(IV, 0, IV.Length);
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception caught in process: {0}", ex);
                    return false;
                }

            }
        }

        static Boolean EncryptFolder(string folderpathN, string folderpathC,  Aes aesAlg)
        {

            foreach (string d in Directory.GetDirectories(folderpathN)) // d = test/...
            {
                int idx = d.LastIndexOf('\\');
                string filename = d.Substring(idx + 1);

                Directory.CreateDirectory(folderpathC + "/" + filename + SUFFIXE_FILE);
                EncryptFolder(d, folderpathC + "/" + filename + SUFFIXE_FILE, aesAlg);
            }

            string[] fileEntries = Directory.GetFiles(folderpathN);



            byte[] key = aesAlg.Key;
            byte[] IV = aesAlg.IV;

            foreach (string fileEntry in fileEntries)
            {
                    
                byte[] encrypted = Encrypt(aesAlg, fileEntry);

                int idx = fileEntry.LastIndexOf('\\');
                string filename = fileEntry.Substring(idx + 1);

                try
                {
                    using (var fs = new FileStream(folderpathC + "/" + filename + SUFFIXE_FILE, FileMode.Create, FileAccess.Write))
                    {
                        fs.Write(encrypted, 0, encrypted.Length);
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception caught in process: {0}", ex);
                    return false;
                }

            }
                
            
                
            return true;
        }

        static Boolean DecryptFile(string filename)
        {
            using (Aes aesAlg = Aes.Create())
            {
                byte[] key = File.ReadAllBytes(filename + SUFFIXE_KEY);
                byte[] IV = File.ReadAllBytes(filename + SUFFIXE_IV);

                try
                {
                    aesAlg.Key = key;
                    aesAlg.IV = IV;
                } catch (Exception ex)
                {
                    Console.WriteLine("Key or vector does not match requirement.");
                    return false;
                }
                

                byte[] decrypted = Decrypt(aesAlg, filename + SUFFIXE_FILE);

                try
                {
                    using (var fs = new FileStream(filename, FileMode.Create, FileAccess.Write))
                    {
                        fs.Write(decrypted, 0, decrypted.Length);
                        return true;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception caught in process: {0}", ex);
                    return false;
                }
            }
        }

        static Boolean DecryptFolder(string folderpathN, string folderpathC, Aes aesAlg)
        {

            foreach (string d in Directory.GetDirectories(folderpathC)) // d = test/...
            {
                int idx = d.LastIndexOf('\\');
                string foldername = d.Substring(idx + 1);

                foldername = foldername.Split(new[] { SUFFIXE_FILE }, StringSplitOptions.None)[0];

                Directory.CreateDirectory(folderpathN + "/" + foldername);
                DecryptFolder(folderpathN + "/" +foldername, d, aesAlg);
            }

            string[] fileEntries = Directory.GetFiles(folderpathC, "*" + SUFFIXE_FILE);


            foreach (string fileEntry in fileEntries)
            {

                byte[] decrypted = Decrypt(aesAlg, fileEntry);

                int idx = fileEntry.LastIndexOf('\\');
                string filename = fileEntry.Substring(idx + 1);

                try
                {
                    filename = filename.Split(new[] { SUFFIXE_FILE }, StringSplitOptions.None)[0];
                    using (var fs = new FileStream(folderpathN + "/" + filename, FileMode.Create, FileAccess.Write))
                    {
                        fs.Write(decrypted, 0, decrypted.Length);
                    }

                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception caught in process: {0}", ex);
                    return false;
                }   

            }

            return true;
        }



        static byte[] Encrypt(SymmetricAlgorithm aesAlg, string filename)
        {
            byte[] byteFichier = File.ReadAllBytes(filename);
            

            if ((byteFichier == null) || (byteFichier.Length == 0))
            {
                return byteFichier;
            }

            if (aesAlg == null)
            {
                throw new ArgumentNullException("alg");
            }

            ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
            using (MemoryStream msEncrypt = new MemoryStream())
            {
                using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                {
                    csEncrypt.Write(byteFichier, 0, byteFichier.Length);
                    csEncrypt.FlushFinalBlock();
                    return msEncrypt.ToArray();
                }

            }

            
        }

        static byte[] Decrypt(SymmetricAlgorithm aesAlg, string filename)
        {
            byte[] byteFichier = File.ReadAllBytes(filename);


            if ((byteFichier == null) || (byteFichier.Length == 0))
            {
                return byteFichier;
            }

            if (aesAlg == null)
            {
                throw new ArgumentNullException("alg");
            }

            ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);
            using (MemoryStream msDecrypt = new MemoryStream())
            {
                using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Write))
                {
                    csDecrypt.Write(byteFichier, 0, byteFichier.Length);
                    csDecrypt.FlushFinalBlock();
                    return msDecrypt.ToArray();
                }

            }


        }

        static bool IsDirectoryEmpty(string path)
        {
            return !Directory.EnumerateFileSystemEntries(path).Any();
        }
    }
}